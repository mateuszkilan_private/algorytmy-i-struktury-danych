#include <iostream>
#include <vector>
#include <conio.h>
#include <cmath>
#include <chrono>


#define NUM_NOT_FOUND 1500000000
using namespace std;

enum LAST_NUM_COMPARISON {
    SMALLER_OR_BIGGER = 0,
    EQUAL = 1,
    NONE = 2
};

long int getSmallestHighestNumberToInputNumber(vector<long int>& holderArray, long int& tempNumHolder) {
    long int leftBoundryIdx = 0;
    long int rightBoundryIdx = holderArray.size() - 1;
    long int middleIdx = floor((float)rightBoundryIdx / (float)2);
    //cout << '\n' << "(" << leftBoundryIdx << "," << rightBoundryIdx << "," << middleIdx << ")" << '\n';
    LAST_NUM_COMPARISON lastHolderArrayNumCompare = NONE;

    if (leftBoundryIdx == rightBoundryIdx && leftBoundryIdx == middleIdx) {
        // it means that we have only one element inside holderArray.
        return holderArray[middleIdx] > tempNumHolder ? holderArray[middleIdx] : NUM_NOT_FOUND;
    }

    if (rightBoundryIdx < 0 || tempNumHolder > holderArray.back()) {
        // it means that we have no elements inside holderArray or tempNumHolder is bigger than biggest holderArray's number
        return NUM_NOT_FOUND;
    }

    while (leftBoundryIdx <= rightBoundryIdx) {
        if (holderArray[middleIdx] > tempNumHolder) {
            // then we go left, because we know that everything that is on the right is bigger including current number
            rightBoundryIdx = middleIdx - 1;
            lastHolderArrayNumCompare = SMALLER_OR_BIGGER;
        }
        else if (holderArray[middleIdx] < tempNumHolder) {
            // we go right, because holderArray number is smaller than tempNumHolder
            leftBoundryIdx = middleIdx + 1;
            lastHolderArrayNumCompare = SMALLER_OR_BIGGER;
        }
        else {
            // we go right, because holderArray's number is equal to tempNumHolder
            leftBoundryIdx = middleIdx + 1;
            lastHolderArrayNumCompare = EQUAL;
        }
    
        middleIdx = floor((float)(leftBoundryIdx + rightBoundryIdx) / (float)2);
    }

    //cout << '\n' << "final idxs (" << leftBoundryIdx << "," << rightBoundryIdx << "," << middleIdx << ")" << '\n';
    //cout << '\n' << "final array size (" << holderArray.size() << '\n';

    //return lastHolderArrayNumCompare == SMALLER_OR_BIGGER ? holderArray[middleIdx + 1] : (holderArray.size() - 1 < middleIdx + 1 ? NUM_NOT_FOUND : holderArray[middleIdx + 1]);
    switch (lastHolderArrayNumCompare) {
    case SMALLER_OR_BIGGER:
        return holderArray[middleIdx + 1];
    case EQUAL:
        return holderArray.size() - 1 < middleIdx + 1 ? NUM_NOT_FOUND : holderArray[middleIdx + 1];
    case NONE:
        return NUM_NOT_FOUND;
    }
}

int main() {
    std::chrono::steady_clock::time_point timeBegin;
    //ios_base::sync_with_stdio(false);
    long int numbersCount = 0;
    long int tempNumHolder = 0;
    long int foundNumber = 0;
    vector<long int> holderArray;

    cin >> numbersCount;
    for (long int iterator = 0; iterator < numbersCount; iterator++) {
        cin >> tempNumHolder;
        timeBegin = std::chrono::steady_clock::now();
        foundNumber = getSmallestHighestNumberToInputNumber(holderArray, tempNumHolder);
        if (foundNumber == NUM_NOT_FOUND) {
            holderArray.push_back(tempNumHolder);
        }
        else {
            cout << foundNumber << " ";
        }        
    }

    //for (int i = 0; i < holderArray.size(); i++) {
    //    cout << '\n' << '\n' << i << ") " << holderArray[i] ;
    //}
    std::chrono::steady_clock::time_point timeEnd = std::chrono::steady_clock::now();
    std::cout << '\n' << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(timeBegin - timeEnd).count() << std::endl;
    _getch();
    return 0;
}

//int main() {
//    //cout.precision(4);
//    //cout << (float)(-1 / 2);
//    //float test = -0.5f;
//    int test = floor((float)-1 / (float)2);
//    //printf("%6.4lf", test);
//    cout << test;
//    _getch();
//    return 0;
//}


// ===================================== ZADANIE - Wyszukiwanie binarne ===================================== 
//long int getCountOfNotSmallerNumbers(long int searchArray[], long int& searchArraySize, long int& number) {
//    long int leftBoundryIdx = 0;
//    long int rightBoundryIdx = searchArraySize - 1;
//    long int middleIdx = rightBoundryIdx / 2;
//    bool lastComparisonNumberWasBigger = false;
//
//    if (number < searchArray[0]) {
//        // then we know that every number in searchArray is bigger than "x" number
//        return searchArraySize;
//    }
//    if (number > searchArray[searchArraySize - 1]) {
//        // then we know that every number in searchArray is smaller than "x" number
//        return 0;
//    }
//
//    while (leftBoundryIdx <= rightBoundryIdx) {
//        if (searchArray[middleIdx] >= number) {
//            // then we go left, because we know that everything that is on the right is bigger including current number
//            rightBoundryIdx = middleIdx - 1;
//            lastComparisonNumberWasBigger = false;
//        }
//        else {
//            // we go right, because current number is smaller than "x" number
//            leftBoundryIdx = middleIdx + 1;
//            lastComparisonNumberWasBigger = true;
//        }
//
//        if (leftBoundryIdx <= rightBoundryIdx) {
//            middleIdx = (leftBoundryIdx + rightBoundryIdx) / 2;
//        }
//    }
//
//    return searchArraySize - (lastComparisonNumberWasBigger ? (middleIdx + 1) : middleIdx);
//}
//
//int main() {
//    long int searchArraySize = 0;
//    long int numReferenceArraySize = 0;
//
//    cin >> searchArraySize;
//    long int* searchArray = new long int[searchArraySize];
//    for (long int iterator = 0; iterator < searchArraySize; iterator++) {
//        cin >> searchArray[iterator];
//    }
//
//    cin >> numReferenceArraySize;
//    long int* numReferenceArray = new long int[numReferenceArraySize];
//    for (long int iterator = 0; iterator < numReferenceArraySize; iterator++) {
//        cin >> numReferenceArray[iterator];
//    }
//
//    for (long int iterator = 0; iterator < numReferenceArraySize;) {
//        cout << getCountOfNotSmallerNumbers(searchArray, searchArraySize, numReferenceArray[iterator]);
//        if (++iterator < numReferenceArraySize) {
//            cout << " ";
//        }
//    }
//
//    //_getch();
//    delete[] searchArray;
//    delete[] numReferenceArray;
//    return 0;
//}

// ===================================== ZADANIE - Co m =====================================
//int main() {
//    int holder;
//    int liczbaElemWTablicy;
//    int offset;
//
//    cin >> liczbaElemWTablicy;
//    int* tablicaLiczb = new int[liczbaElemWTablicy];
//
//    for (int i = 0; i < liczbaElemWTablicy; i++) {
//        cin >> holder;
//        tablicaLiczb[i] = holder;
//    }
//
//    cin >> offset;
//
//    for (int i = 0; i < liczbaElemWTablicy;) {
//        cout << tablicaLiczb[i];
//        i += offset;
//        if (i < liczbaElemWTablicy) {
//            cout << " ";
//        }
//    }
//
//    delete[] tablicaLiczb;
//    return 0;
//}
